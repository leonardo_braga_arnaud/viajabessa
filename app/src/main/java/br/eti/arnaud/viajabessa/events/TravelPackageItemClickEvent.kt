package br.eti.arnaud.viajabessa.events

import br.eti.arnaud.viajabessa.models.TravelPackages

data class TravelPackageItemClickEvent(val position: Int,
                                       val travelPackage: TravelPackages.TravelPackage)
