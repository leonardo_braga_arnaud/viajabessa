package br.eti.arnaud.viajabessa.events

import br.eti.arnaud.viajabessa.models.TravelPackages

data class TravelPackageListEvent(val travelPackages: List<TravelPackages.TravelPackage>)
