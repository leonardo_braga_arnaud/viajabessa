package br.eti.arnaud.viajabessa

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.eti.arnaud.viajabessa.events.TravelPackageDetailEvent
import br.eti.arnaud.viajabessa.models.TravelPackages
import kotlinx.android.synthetic.main.fragment_travelpackage_detail.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class TravelPackageDetailFragment: Fragment() {

    private lateinit var travelPackage: TravelPackages.TravelPackage

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_travelpackage_detail, container, false)
    }

    override fun onStart() {
        super.onStart()

        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()

        EventBus.getDefault().unregister(this)
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: TravelPackageDetailEvent) {
        travelPackage = event.travelPackage
        setupContent()
    }

    private fun setupContent() {
        priceTextView.text = travelPackage.getFormattedValue()
        descriptionTextView.text = travelPackage.description
    }

    companion object {

        fun instantiate(supportFragmentManager: FragmentManager, layoutId: Int) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(layoutId, TravelPackageDetailFragment())
                    .commit()
        }
    }
}
