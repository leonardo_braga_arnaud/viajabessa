package br.eti.arnaud.viajabessa

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.eti.arnaud.viajabessa.events.TravelPackageListEvent
import br.eti.arnaud.viajabessa.events.TravelPackagesRefreshEvent
import br.eti.arnaud.viajabessa.models.TravelPackages
import kotlinx.android.synthetic.main.fragment_travelpackage_list.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class TravelPackageListFragment: Fragment() {
    private lateinit var travelPackages: List<TravelPackages.TravelPackage>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_travelpackage_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupLayoutManager()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()

        EventBus.getDefault().unregister(this)
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: TravelPackageListEvent) {
        travelPackages = event.travelPackages
        setupList()
    }

    private fun setupLayoutManager() {
        val columnCount = resources.getInteger(R.integer.list_column_count)
        travelPackagesRecyclerView.layoutManager =
                if (columnCount <= 1) LinearLayoutManager(context)
                else GridLayoutManager(context, columnCount)
    }

    private fun setupList() {
        travelPackagesRecyclerView.adapter = TravelPackageListRecyclerViewAdapter(travelPackages)
        swipeRefreshLayout.setOnRefreshListener {
            EventBus.getDefault().post(TravelPackagesRefreshEvent())
        }
    }

    companion object {

        fun instantiate(supportFragmentManager: FragmentManager, layoutId: Int) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(layoutId, TravelPackageListFragment())
                    .commit()
        }
    }
}
