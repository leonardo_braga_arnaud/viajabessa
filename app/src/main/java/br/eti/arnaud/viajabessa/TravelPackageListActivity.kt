package br.eti.arnaud.viajabessa

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import br.eti.arnaud.viajabessa.events.TravelPackageDetailEvent
import br.eti.arnaud.viajabessa.events.TravelPackageItemClickEvent
import br.eti.arnaud.viajabessa.events.TravelPackageListEvent
import br.eti.arnaud.viajabessa.events.TravelPackagesRefreshEvent
import br.eti.arnaud.viajabessa.interfaces.ViajabessaApi
import br.eti.arnaud.viajabessa.models.DeviceInfo
import br.eti.arnaud.viajabessa.models.TravelPackages
import kotlinx.android.synthetic.main.activity_travelpackage_list.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class TravelPackageListActivity : AppCompatActivity() {

    private var travelPackagesCall: Call<TravelPackages>? = null
    private lateinit var retrofit: Retrofit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_travelpackage_list)
        setSupportActionBar(toolbar)

        buyFab?.setOnClickListener{
            Toast.makeText(this@TravelPackageListActivity, "Comprar", Toast.LENGTH_LONG)
                    .show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onStart() {
        super.onStart()

        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()

        EventBus.getDefault().unregister(this)

        travelPackagesCall?.cancel()
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onMessageEvent(retrofit: Retrofit) {
        this@TravelPackageListActivity.retrofit = retrofit
        requestPackages()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: TravelPackagesRefreshEvent) {
        requestPackages()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: TravelPackageItemClickEvent) {
        val travelPackageDetailEvent = TravelPackageDetailEvent(event.travelPackage)
        EventBus.getDefault().postSticky(travelPackageDetailEvent)
        if (detailContainerFrameLayout != null){
            inviteTextView?.visibility = View.GONE
            buyFab?.show()
            TravelPackageDetailFragment.instantiate(supportFragmentManager,
                    R.id.detailContainerFrameLayout)
        } else {
            TravelPackageDetailActivity.start(this)
        }
    }

    private fun requestPackages() {
        travelPackagesCall = retrofit.create(ViajabessaApi::class.java).travelPackages(DeviceInfo())
        travelPackagesCall?.enqueue(TravelPackagesCallback())
    }

    companion object {

        fun start(context: Context) {
            val intent = Intent(context, TravelPackageListActivity::class.java)
            context.startActivity(intent)
        }
    }

    inner class TravelPackagesCallback: Callback<TravelPackages>{

        override fun onResponse(call: Call<TravelPackages>?, response: Response<TravelPackages>?) {
            if (response?.isSuccessful == true) {
                val travelPackages = response.body()?.data

                val event = TravelPackageListEvent(travelPackages!!)
                EventBus.getDefault().postSticky(event)
                TravelPackageListFragment.instantiate(supportFragmentManager,
                        R.id.listContainerFrameLayout)
            }
        }

        override fun onFailure(call: Call<TravelPackages>?, t: Throwable?) {
            Snackbar.make(listContainerFrameLayout, R.string.api_error, Snackbar.LENGTH_LONG).show()
        }

    }
}
