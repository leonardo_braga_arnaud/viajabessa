package br.eti.arnaud.viajabessa

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.eti.arnaud.viajabessa.events.TravelPackageItemClickEvent
import br.eti.arnaud.viajabessa.models.TravelPackages
import com.bumptech.glide.Glide
import org.greenrobot.eventbus.EventBus

class TravelPackageListRecyclerViewAdapter(private val values:List<TravelPackages.TravelPackage>):
        RecyclerView.Adapter<TravelPackageListRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_travelpackage, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item = values[position]

        Glide.with(holder.itemView.context)
                .load(values[position].image)
                .into(holder.packageImageView)

        holder.titleTextView.text = values[position].title
        holder.priceTextView.text = values[position].getFormattedValue()

        holder.view.setOnClickListener {
            EventBus.getDefault().post(TravelPackageItemClickEvent(position, holder.item!!))
        }
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        Glide.with(holder.itemView.context).clear(holder.packageImageView)

    }

    override fun getItemCount():Int {
        return values.size
    }

    inner class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        val packageImageView = view.findViewById(R.id.packageImageView) as ImageView
        val titleTextView = view.findViewById(R.id.titleTextView) as TextView
        val priceTextView = view.findViewById(R.id.priceTextView) as TextView
        var item: TravelPackages.TravelPackage? = null
    }
}
