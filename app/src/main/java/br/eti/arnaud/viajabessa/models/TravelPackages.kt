package br.eti.arnaud.viajabessa.models

import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

data class TravelPackages (val message: String, val data: List<TravelPackage>){

    data class TravelPackage(val title: String,
                             private val value: Double,
                             val image: String,
                             val description: String){

        fun getFormattedValue(): String {
            val formatter = NumberFormat.getCurrencyInstance(
                    Locale.getDefault()) as DecimalFormat
            val symbols = formatter.decimalFormatSymbols
            symbols.currencySymbol = "R$ "
            formatter.decimalFormatSymbols = symbols
            formatter.maximumFractionDigits = 2
            formatter.minimumFractionDigits = 2
            return formatter.format(value)
        }

    }
}