package br.eti.arnaud.viajabessa.interfaces

import br.eti.arnaud.viajabessa.models.DeviceInfo
import br.eti.arnaud.viajabessa.models.TravelPackages
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ViajabessaApi {

    @POST("travel_packages")
    fun travelPackages(@Body deviceInfo: DeviceInfo): Call<TravelPackages>

}
