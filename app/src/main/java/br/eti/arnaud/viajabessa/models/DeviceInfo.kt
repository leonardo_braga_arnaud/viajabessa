package br.eti.arnaud.viajabessa.models

import android.os.Build

class DeviceInfo{
    private var manufacturer: String = Build.MANUFACTURER
    private var model: String = Build.MODEL
    private var version: Int = Build.VERSION.SDK_INT
    private var versionRelease: String = Build.VERSION.RELEASE
}

