package br.eti.arnaud.viajabessa

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import br.eti.arnaud.viajabessa.events.TravelPackageDetailEvent
import br.eti.arnaud.viajabessa.models.TravelPackages
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_travelpackage_detail.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class TravelPackageDetailActivity : AppCompatActivity() {

    private lateinit var travelPackage: TravelPackages.TravelPackage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_travelpackage_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setupBuyButton()
    }

    override fun onStart() {
        super.onStart()

        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()

        EventBus.getDefault().unregister(this)
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: TravelPackageDetailEvent) {
        travelPackage = event.travelPackage
        setupTravelPackageDetail()
    }

    private fun setupBuyButton() {
        val buyClick = View.OnClickListener{
            Toast.makeText(this, "Comprar", Toast.LENGTH_SHORT).show()
        }
        topBuyFab.setOnClickListener(buyClick)
        bottomBuyFab.setOnClickListener(buyClick)

        appBarLayout.addOnOffsetChangedListener({ _, verticalOffset ->
            setupBuyButtonShowing(verticalOffset)
        })
    }

    private fun setupBuyButtonShowing(verticalOffset: Int) {
        var verticalDiff = Math.abs(verticalOffset) - appBarLayout.totalScrollRange
        val changeFabLimit = resources.getDimensionPixelSize(R.dimen.change_fab_limit)
        if (verticalDiff > changeFabLimit) verticalDiff = 0
        if (verticalDiff == 0) bottomBuyFab.show()
        else bottomBuyFab.hide()
    }

    private fun setupTravelPackageDetail() {
        title = travelPackage.title
        Glide.with(this)
                .load(travelPackage.image)
                .into(packageImageView)

        TravelPackageDetailFragment.instantiate(supportFragmentManager,
                R.id.detailContainerFrameLayout)
    }

    companion object {

        fun start(context: Context) {
            val intent = Intent(context, TravelPackageDetailActivity::class.java)
            context.startActivity(intent)

        }
    }
}
