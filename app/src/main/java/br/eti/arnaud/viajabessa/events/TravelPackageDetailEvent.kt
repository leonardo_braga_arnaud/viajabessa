package br.eti.arnaud.viajabessa.events

import br.eti.arnaud.viajabessa.models.TravelPackages

data class TravelPackageDetailEvent(val travelPackage: TravelPackages.TravelPackage)
